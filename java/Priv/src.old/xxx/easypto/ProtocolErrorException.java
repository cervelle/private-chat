package xxx.easypto;

public class ProtocolErrorException extends RuntimeException {

  private static final long serialVersionUID = -6500326662291483634L;

  public ProtocolErrorException() {
  }

  public ProtocolErrorException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public ProtocolErrorException(String message, Throwable cause) {
    super(message, cause);
  }

  public ProtocolErrorException(String message) {
    super(message);
  }

  public ProtocolErrorException(Throwable cause) {
    super(cause);
  }

}
