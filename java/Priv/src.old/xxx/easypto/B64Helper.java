package xxx.easypto;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.regex.Pattern;

import xxx.easypto.packet.MalformedPacketException;
import xxx.easypto.packet.Packet;

public class B64Helper {
  private static ByteBuffer buffer = ByteBuffer.allocateDirect(2<<20);
  
  private static final Pattern pattern = Pattern.compile("\\p{Space}*<\\*>[a-zA-Z0-9/+]*=*<\\*>\\p{Space}*");
  
  public static boolean isCoding(String s) {
    return pattern.matcher(s).matches();
  }
  
  public static String serializePacket(Packet p) {
    while(true) {
      try {
        p.encode(buffer);
        break;
      }
      catch
      (BufferOverflowException e) {
        //System.out.println(buffer.capacity());
        buffer = ByteBuffer.allocateDirect(buffer.capacity()*2);
      }
    }
    buffer.flip();
    ByteBuffer base64 = Base64.getEncoder().encode(buffer);
    StringBuilder b = new StringBuilder("<*>");
    while(base64.hasRemaining())
      b.append((char)base64.get());
    b.append("<*>");
    buffer.clear();
    //System.out.println(b.toString());
    return b.toString();
  }
  
  public static Packet deserializePacket(String s) throws MalformedPacketException {
    if (!isCoding(s))
      throw new IllegalArgumentException();
    s=s.trim();
    if (buffer.capacity()<s.length()-6)
      buffer = ByteBuffer.allocateDirect(s.length()-6);
    
    for(int i=3;i<s.length()-3;i++) {
      buffer.put((byte)s.charAt(i));
    }
    
    buffer.flip();
    
    try {
      return Packet.decode(Base64.getDecoder().decode(buffer));
    }
    catch (IllegalArgumentException e) {
      throw new MalformedPacketException(e);
    }
    finally {
      buffer.clear();
    }
  }
}
