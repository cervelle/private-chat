package xxx.easypto;

import java.util.LinkedList;
import java.util.Random;

import xxx.easypto.packet.MalformedPacketException;
import xxx.easypto.packet.Packet;

public class TestDirect {

  private static LinkedList<String> a2b = new LinkedList<>();
  private static LinkedList<String> b2a = new LinkedList<>();
  private static PacketManagement a = new PacketManagement();
  private static PacketManagement b = new PacketManagement();
  
  public static void main(String[] args) throws InterruptedException, MalformedPacketException {
    

    

    Random RND = new Random();
    long l = System.nanoTime();
    RND.setSeed(l);
    System.out.println("seed = "+l);

    a.init(TestDirect::sendA, TestDirect::printA);
    b.init(TestDirect::sendB, TestDirect::printB);
    a.startHandshake();
    b.startHandshake();
    receiveA();
    receiveB();
    
    for (;;) {
      switch(RND.nextInt(4)) {
      case 0: b.sendString("blibli");break;
      case 1: a.sendString("Coucou");break;
      case 2: receiveA();
      case 3: receiveB();
      }
    }
  }
  
  private static void sendA(Packet s) {
    a2b.addFirst(B64Helper.serializePacket(s));
  }
  
  private static void sendB(Packet s) {
    b2a.addFirst(B64Helper.serializePacket(s));
  }
  
  private static void receiveA() throws MalformedPacketException {
    if (!b2a.isEmpty())
      a.receive(B64Helper.deserializePacket(b2a.removeLast()));
  }
  
  private static void receiveB() throws MalformedPacketException {
    if (!a2b.isEmpty())
      b.receive(B64Helper.deserializePacket(a2b.removeLast()));
  }
  
  private static void printA(String s) {
    System.out.println("A: "+s);
    if (!(s.startsWith("DBG :")||s.equals("blibli")))
      System.exit(1);
  }
  
  private static void printB(String s) {
    System.out.println("B:                                    "+s);
    if (!(s.startsWith("DBG :")||s.equals("Coucou")))
      System.exit(1);
  }
}
