package xxx.easypto.packet;

import java.nio.ByteBuffer;
import java.security.PublicKey;

import xxx.easypto.Crypto;

public class HelloPacket extends Packet {

  @Override
  public PacketType getType() {
    return PacketType.HELLO;
  }
  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(packetNumber);
    buffer.put(iv);
    byte[] keyBytes = key.getEncoded();
    buffer.put(keyBytes);
  }
  
  public HelloPacket(int packetNumber, PublicKey key, byte[] iv) {
    this.packetNumber = packetNumber;
    this.key = key;
    this.iv=iv.clone();
  }

  public PublicKey getKey() {
    return key;
  }
  
  public byte[] getIV() {
    return iv;
  }
  
  public int getPacketNumber() {
    return packetNumber;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int packetNumber = buffer.getInt();
    byte[] iv = Crypto.emptyIV();
    buffer.get(iv);
    byte[] keyBytes = new byte[buffer.remaining()];
    buffer.get(keyBytes);
    return new HelloPacket(packetNumber,Crypto.decodeKey(keyBytes),iv);
  }

  private final PublicKey key;
  private final byte[] iv;
  private final int packetNumber;

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getHello().accept(this);
  }

}
