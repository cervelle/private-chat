package xxx.easypto.packet;

import java.nio.ByteBuffer;

public class AckPacket extends DataPacket {
  
  
  
  public AckPacket(int packetNumber, byte[] message) {
    super(packetNumber, message);
  }


  @Override
  public PacketType getType() {
    return PacketType.ACK;
  }

  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getPacketNumber());
    buffer.put(getMessage());
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int packetNumber = buffer.getInt();
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new AckPacket(packetNumber,message);
  }

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getAck().accept(this);
  }
  
  

}
