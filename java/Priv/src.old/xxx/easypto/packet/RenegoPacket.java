package xxx.easypto.packet;

import java.nio.ByteBuffer;
import java.security.PublicKey;

import xxx.easypto.Crypto;

public class RenegoPacket extends DataPacket {

  @Override
  public PacketType getType() {
    return PacketType.RENEGO;
  }
  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getPacketNumber());
    buffer.put(iv);
    byte[] keyBytes = key.getEncoded();
    buffer.put((byte)keyBytes.length);
    buffer.put(keyBytes);
    buffer.put(getMessage());
  }
  
  public RenegoPacket(int packetNumber, PublicKey key, byte[] iv, byte[] message) {
    super(packetNumber,message);
    this.key = key;
    this.iv=iv.clone();
  }

  public PublicKey getKey() {
    return key;
  }
  
  public byte[] getIV() {
    return iv;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int packetNumber = buffer.getInt();
    byte[] iv = Crypto.emptyIV();
    buffer.get(iv);
    int keyLength = buffer.get();
    byte[] keyBytes = new byte[keyLength];               
    buffer.get(keyBytes);
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new RenegoPacket(packetNumber,Crypto.decodeKey(keyBytes),iv,message);
  }

  private final PublicKey key;
  private final byte[] iv;

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getRenego().accept(this);
  }

}
