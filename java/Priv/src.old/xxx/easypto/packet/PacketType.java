package xxx.easypto.packet;

public enum PacketType {
  HELLO(1,HelloPacket::decodeImpl),MSG(2,MsgPacket::decodeImpl),ERR(3,ErrPacket::decodeImpl),
  RENEGO(4,RenegoPacket::decodeImpl),ACK(5,AckPacket::decodeImpl),RENEGO_ACK(6,RenegoAckPacket::decodeImpl);
  
  PacketType(int code, PacketFactory factory) {
    this.code=(byte)code;
    this.factory=factory;
  }
  public byte getCode() {
    return code;
  }
  public PacketFactory getFactory() {
    return factory;
  }
  
  public static PacketType ofCode(byte b) {
    for(PacketType p:values()) {
      if(p.getCode()==b)
        return p;
    }
    return null;
  }
  
  private final byte code;
  private final PacketFactory factory;
}
