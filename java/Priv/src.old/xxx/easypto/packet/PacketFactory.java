package xxx.easypto.packet;

import java.nio.ByteBuffer;

public interface PacketFactory {
  Packet decode(ByteBuffer buffer) throws MalformedPacketException;
}
