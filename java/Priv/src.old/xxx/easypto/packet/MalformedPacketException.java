package xxx.easypto.packet;

public class MalformedPacketException extends Exception {

  private static final long serialVersionUID = -5220584625703407369L;

  public MalformedPacketException() {
  }

  public MalformedPacketException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public MalformedPacketException(String message, Throwable cause) {
    super(message, cause);
  }

  public MalformedPacketException(String message) {
    super(message);
  }

  public MalformedPacketException(Throwable cause) {
    super(cause);
  }
   
}
