package xxx.easypto.packet;

import java.nio.ByteBuffer;

public class MsgPacket extends DataPacket {
  
  
  
  public MsgPacket(int packetNumber, byte[] message) {
    super(packetNumber, message);
  }


  @Override
  public PacketType getType() {
    return PacketType.MSG;
  }

  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getPacketNumber());
    buffer.put(getMessage());
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int packetNumber = buffer.getInt();
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new MsgPacket(packetNumber,message);
  }

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getMsg().accept(this);
  }

}
