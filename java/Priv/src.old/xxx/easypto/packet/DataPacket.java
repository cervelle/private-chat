package xxx.easypto.packet;

public abstract class DataPacket extends Packet {

  public byte[] getMessage() {
    return message;
  }
  
  public int getPacketNumber() {
    return packetNumber;
  }

  public DataPacket(int packetNumber, byte[] message) {
    this.message = message;
    this.packetNumber = packetNumber;
  }

  private final byte[] message;
  private final int packetNumber;
  
}
