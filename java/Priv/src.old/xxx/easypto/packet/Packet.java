package xxx.easypto.packet;

import java.nio.ByteBuffer;

public abstract class Packet {
    public abstract PacketType getType();
    public abstract void encodeImpl(ByteBuffer buffer);
    public final void encode(ByteBuffer buffer) {
      buffer.put(getType().getCode());
      encodeImpl(buffer);
    }
    
    public abstract void accept(PacketVisitor visitor);
    
    public static Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      PacketType type = PacketType.ofCode(buffer.get());
      if (type==null)
        throw new MalformedPacketException("unknown packet code");
      return type.getFactory().decode(buffer);
    }
    
}
