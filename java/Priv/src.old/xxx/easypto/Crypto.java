package xxx.easypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
  private static final KeyFactory keyFactory;
  private static final SecureRandom RND;

  /*
  try {
    s = UTF8.newDecoder()
        .onMalformedInput(CodingErrorAction.REPORT)
        .onUnmappableCharacter(CodingErrorAction.REPORT)
        .decode(buffer).toString();
  } catch (CharacterCodingException e) {*/
  
  static {
    try {
      keyFactory = KeyFactory.getInstance("EC");
      RND = SecureRandom.getInstance("SHA1PRNG");
    } catch (NoSuchAlgorithmException e) {
      throw new AssertionError("JVM badly installed", e);
    }
  }

  public static PublicKey decodeKey(byte[] keyBytes) {
    PublicKey privateKey;
    try {
      privateKey = keyFactory.generatePublic(new X509EncodedKeySpec(keyBytes));
    } catch (InvalidKeySpecException e) {
      throw new AssertionError("JVM badly installed", e);
    }
    return privateKey;
  }

  public static KeyPair generateKeyPair() {
    KeyPairGenerator keyGenerator;
    try {
      keyGenerator = KeyPairGenerator.getInstance("EC");
    } catch (NoSuchAlgorithmException e) {
      throw new AssertionError("JVM badly installed", e);
    }
    keyGenerator.initialize(256, RND);
    return keyGenerator.generateKeyPair();
  }

  public static SecretKey makeKey(PrivateKey privateKey, PublicKey publicKey) {
    KeyAgreement keyAgreement;
    try {
      keyAgreement = KeyAgreement.getInstance("ECDH");
      keyAgreement.init(privateKey, RND);
      keyAgreement.doPhase(publicKey, true);
    } catch (NoSuchAlgorithmException | InvalidKeyException e) {
      throw new AssertionError("JVM badly installed", e);
    }
    return new SecretKeySpec(keyAgreement.generateSecret(),"AES");
  }

  public static void fillIV(byte[] iv) {
    RND.nextBytes(iv);
  }

  public static Cipher initCipher(SecretKey aesSecret, byte[] iv, int encryptMode) {
    Cipher cipher;
    try {
      cipher = Cipher.getInstance("AES/CTR/NoPadding");
      IvParameterSpec ips = new IvParameterSpec(iv);
      cipher.init(encryptMode, aesSecret, ips, RND);
      return cipher;
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
      throw new AssertionError("JVM badly installed", e);
    }
  }

  public static int nextInt() {
    return RND.nextInt();
  }

  public static byte[] emptyIV() {
    return new byte[256/16];
  }

  public static int keySize() {
    return 256;
  }
}
