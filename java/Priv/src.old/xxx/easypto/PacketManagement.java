package xxx.easypto;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.function.Consumer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;

import xxx.easypto.packet.DataPacket;
import xxx.easypto.packet.ErrPacket;
import xxx.easypto.packet.HelloPacket;
import xxx.easypto.packet.AckPacket;
import xxx.easypto.packet.MsgPacket;
import xxx.easypto.packet.Packet;
import xxx.easypto.packet.PacketVisitor;
import xxx.easypto.packet.RenegoAckPacket;
import xxx.easypto.packet.RenegoPacket;

public class PacketManagement {
  private int negoPacketNumber;
  private int packetNumber;
  private int lastReceived;

  private boolean handshakeCalled;

  private enum RenegoState {
    DEFAULT, RENEGO_SENT, ACK_TO_BE_SENT, RENEGO_ACK_TO_BE_SENT, ACK_EXPECTED, CROSS_RENEGO
  }

  private RenegoState renegoState = RenegoState.DEFAULT;

  private SecretKey key;
  private byte[] ivEgress, ivIngress;
  private PrivateKey privateKey;
  private PublicKey publicKey;
  private Cipher cipherIngress;
  private Cipher cipherEgress;

  private Consumer<Packet> emit;
  private Consumer<String> print;

  private final PacketVisitor visitor = new PacketVisitor(this::receiveHello,
      this::receiveMsg, this::receiveErr, this::receiveRenego,
      this::receiveRenegoAck, this::receiveAck);

  /* API */

  public void init(Consumer<Packet> emit, Consumer<String> print) {
    this.emit = emit;
    this.print = print;
    ivEgress = Crypto.emptyIV();
    packetNumber = Crypto.nextInt();
  }

  public void receive(Packet packet) {
    debug("recv "+packet.getType().name());    
    packet.accept(visitor);
  }

  /* utils */

  private void emit(Packet packet) {
    debug("send "+packet.getType().name());
    emit.accept(packet);
  }

  private void print(String s) {
    print.accept(s);
  }

  private byte[] encodeUTF8(String s) {
    ByteBuffer buffer = StandardCharsets.UTF_8.encode(s);
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return message;
  }

  /* crypto */

  private void computeKey() {
    key = Crypto.makeKey(privateKey, publicKey);
    debug("Key "+(key.getEncoded()[0]&0xFF));
  }

  private void setCipherEgress() {
    debug("E "+(key.getEncoded()[0]&0xFF)+" "+(ivEgress[0]&0xFF));
    cipherEgress = Crypto.initCipher(key, ivEgress, Cipher.ENCRYPT_MODE);
  }

  private void setCipherIngress() {
    debug("I "+(key.getEncoded()[0]&0xFF)+" "+(ivIngress[0]&0xFF));
    cipherIngress = Crypto.initCipher(key, ivIngress, Cipher.DECRYPT_MODE);
  }

  private boolean receiveData(DataPacket packet, boolean last) {
    byte[] cipheredMessage = packet.getMessage();
    int number = packet.getPacketNumber();
    if (number != lastReceived + 1) {
      /* throw new UnsupportedOperationException(
          "Received packet " + number + " expecting " + (lastReceived + 1)); */
      print("/!\\ Misssing packets /!\\");
      emit(new ErrPacket(lastReceived,number));
      return false;
    }
    lastReceived++;
    byte[] message;
    if (last)
      try {
        message = cipherIngress.doFinal(cipheredMessage);
      } catch (IllegalBlockSizeException | BadPaddingException e) {
        throw new ProtocolErrorException(e);
      }
    else
      message = cipherIngress.update(cipheredMessage);
    print(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(message)).toString());
    return true;
  }

  /* Default message part */

  public void receiveMsg(MsgPacket packet) {
    receiveData(packet, false);
  }

  /* Hello part */

  public void startHandshake() {
    if (handshakeCalled)
      throw new ProtocolErrorException("Starting handshake twice");
    handshakeCalled = true;
    performHandshake();
  }

  private void performHandshake() {
    if (key != null) {
      /*
       * startHandShake called while nego already finished because Hello packet
       * was received
       */
      return;
    }

    if (privateKey != null)
      throw new AssertionError();

    KeyPair pair = Crypto.generateKeyPair();
    negoPacketNumber = packetNumber;
    privateKey = pair.getPrivate();
    Crypto.fillIV(ivEgress);
    emit(new HelloPacket(packetNumber, pair.getPublic(), ivEgress));
  }

  public void receiveHello(HelloPacket packet) throws ProtocolErrorException {
    if (key != null)
      throw new ProtocolErrorException("Received new handshake");

    if (privateKey == null) { /*
                               * packet received before startHandShake is called
                               */
      performHandshake();
    }
    lastReceived = packet.getPacketNumber();
    ivIngress = packet.getIV();
    publicKey = packet.getKey();
    computeKey();
    setCipherEgress();
    setCipherIngress();
  }

  /* Renego part */

  public void receiveRenego(RenegoPacket packet) {
    if (!receiveData(packet, false))
      return;
    ivIngress = packet.getIV();
    publicKey = packet.getKey();
    
    switch (renegoState) {
    case RENEGO_SENT:
      renegoState = RenegoState.CROSS_RENEGO;
      computeKey();
      break;
    case DEFAULT:
      renegoState = RenegoState.RENEGO_ACK_TO_BE_SENT;
      break;
    default:
      throw new ProtocolErrorException(renegoState.name());
    }


  }

  public void receiveRenegoAck(RenegoAckPacket packet) {
    if (!receiveData(packet, true))
        return;

    if (renegoState != RenegoState.RENEGO_SENT)
      throw new ProtocolErrorException();
    renegoState = RenegoState.ACK_TO_BE_SENT;

    ivIngress = packet.getIV();
    publicKey = packet.getKey();
    computeKey();
    setCipherIngress();
  }

  public void receiveAck(AckPacket ack) {
    switch (renegoState) {
    case CROSS_RENEGO:
      if (!receiveData(ack, false))
        return;
      renegoState = RenegoState.ACK_TO_BE_SENT;
      setCipherIngress();
      break;
    case ACK_EXPECTED:
      if (!receiveData(ack, true))
        return;
      renegoState = RenegoState.DEFAULT;
      setCipherIngress();
      break;
    default:
      throw new ProtocolErrorException();
    }
  }

  /* Error handling */

  public void receiveErr(ErrPacket packet) {
   int x = packet.getReceivedMessage()-packet.getLastDecryptedMessage()-1;
   if (x==1)
     print("/!\\ lacking 1 message");
   else
     print("/!\\ lacking "+x+" messages");
  }

  /* Sending messages */

  public void sendString(String s) {
    debug("DBG : send "+s);
    if (!handshakeCalled)
      throw new IllegalStateException("startHandshake not called");
    if (key==null) {
      throw new IllegalStateException("handshake not finished");
    }

    byte[] message = encodeUTF8(s);
    byte[] cipheredMessage;
    
    switch(renegoState) {
    case DEFAULT:
    case RENEGO_SENT:
    case ACK_EXPECTED:
      cipheredMessage = cipherEgress.update(message);
      break;
    case ACK_TO_BE_SENT:
    case CROSS_RENEGO:
    case RENEGO_ACK_TO_BE_SENT:
      try {
        cipheredMessage = cipherEgress.doFinal(message);
      } catch (IllegalBlockSizeException | BadPaddingException e) {
        throw new ProtocolErrorException(e);
      }
      break;
    default:
      throw new AssertionError();
    }
    packetNumber++;
    
    switch(renegoState) {
    case DEFAULT:
      if (renegociationWanted()) {
        KeyPair pair = Crypto.generateKeyPair();
        negoPacketNumber = packetNumber;
        privateKey = pair.getPrivate();
        Crypto.fillIV(ivEgress);
        renegoState = RenegoState.RENEGO_SENT;
        emit(new RenegoPacket(packetNumber, pair.getPublic() , ivEgress, cipheredMessage));
        break;
      }
      // else proceed to default behavior identical to the next two cases
    case RENEGO_SENT:
    case ACK_EXPECTED:
      emit(new MsgPacket(packetNumber, cipheredMessage));
      break;
    case ACK_TO_BE_SENT:
      setCipherEgress();
      renegoState = RenegoState.DEFAULT;
      emit(new AckPacket(packetNumber, cipheredMessage));
      break;
    case CROSS_RENEGO:
      setCipherEgress();
      renegoState = RenegoState.ACK_EXPECTED;
      emit(new AckPacket(packetNumber, cipheredMessage));
      break;
    case RENEGO_ACK_TO_BE_SENT:
      KeyPair pair = Crypto.generateKeyPair();
      negoPacketNumber = packetNumber;
      privateKey = pair.getPrivate();
      Crypto.fillIV(ivEgress);
      computeKey();
      setCipherEgress();
      renegoState = RenegoState.ACK_EXPECTED;
      emit(new RenegoAckPacket(packetNumber, pair.getPublic() , ivEgress,cipheredMessage));
      break;
    default:
       throw new AssertionError(); 
    }
    
  }
  
  private void debug(String s) {
    // print("DBG : "+s+" ["+renegoState+"]");
  }

  private boolean renegociationWanted() {
    boolean renego = (long) (packetNumber) - (long) (negoPacketNumber) > 4L;
    if (renego)
     debug("renego");
    return renego;
  }

}
