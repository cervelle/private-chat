package xxx.easypto.priv;

import java.util.Scanner;

import xxx.easypto.priv.packet.MalformedPacketException;
import xxx.easypto.priv.protocol.PacketManagement;

public class Main {
  public static void main(String[] args) throws MalformedPacketException {
    PacketManagement pm = new PacketManagement();
    try (Scanner scanner = new Scanner(System.in)) {
      pm.init(p -> System.out.println("    "+B64Helper.serializePacket(p)),
          System.out::println);
      pm.startHandshake();
      while (scanner.hasNextLine()) {
        String s = scanner.nextLine();
        if (B64Helper.isCoding(s))
          pm.receive(B64Helper.deserializePacket(s));
        else
          pm.sendString(s);
      }
    }
  }
}
