package xxx.easypto.priv.packet;

/**
 * Created by cervelle on 19/05/2017.
 */

public interface Consumer<T> {

    void accept(T t);
}
