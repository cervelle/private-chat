package xxx.easypto.priv.packet;

public class PacketVisitor {
  private final Consumer<HelloPacket> hello;
  private final Consumer<MsgPacket> msg;
  private final Consumer<ErrPacket> err;
  private final Consumer<RenegoPacket> renego;
  private final Consumer<RenegoAckPacket> renegoAck;
  private final Consumer<AckPacket> ack;


  public PacketVisitor(Consumer<HelloPacket> hello, Consumer<MsgPacket> msg,
      Consumer<ErrPacket> err, Consumer<RenegoPacket> renego,
      Consumer<RenegoAckPacket> renegoAck, Consumer<AckPacket> ack) {
    this.hello = hello;
    this.msg = msg;
    this.err = err;
    this.renego = renego;
    this.renegoAck = renegoAck;
    this.ack = ack;
  }
  public Consumer<HelloPacket> getHello() {
    return hello;
  }
  public Consumer<MsgPacket> getMsg() {
    return msg;
  }
  public Consumer<ErrPacket> getErr() {
    return err;
  }
  public Consumer<RenegoPacket> getRenego() {
    return renego;
  }
  public Consumer<AckPacket> getAck() {
    return ack;
  }
  public Consumer<RenegoAckPacket> getRenegoAck() {
    return renegoAck;
  }
}
