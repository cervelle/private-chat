package xxx.easypto.priv.packet;

public abstract class DataPacket extends Packet {

  public byte[] getMessage() {
    return message;
  }
  
  public int getPacketNumber() {
    return packetNumber;
  }
  
  public int getKeyNumber() {
    return keyNumber;
  }

  public DataPacket(int keyNumber, int packetNumber, byte[] message) {
    this.message = message;
    this.packetNumber = packetNumber;
    this.keyNumber = keyNumber;
  }

  private final byte[] message;
  private final int packetNumber;
  private final int keyNumber;
  
}
