package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;

public class MsgPacket extends DataPacket {
  
  
  
  public MsgPacket(int keyNumber, int packetNumber, byte[] message) {
    super(keyNumber, packetNumber, message);
  }


  @Override
  public PacketType getType() {
    return PacketType.MSG;
  }

  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getKeyNumber());
    buffer.putInt(getPacketNumber());
    buffer.put(getMessage());
  }

  public int serializationSize() {
    return 1+4+4+getMessage().length;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int keyNumber = buffer.getInt();
    int packetNumber = buffer.getInt();
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new MsgPacket(keyNumber,packetNumber,message);
  }

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getMsg().accept(this);
  }

}
