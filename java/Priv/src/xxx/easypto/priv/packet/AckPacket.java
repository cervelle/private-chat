package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;

public class AckPacket extends DataPacket {
  
  
  
  public AckPacket(int keyNumber, int packetNumber, byte[] message) {
    super(keyNumber, packetNumber, message);
  }


  @Override
  public PacketType getType() {
    return PacketType.ACK;
  }

  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getKeyNumber());
    buffer.putInt(getPacketNumber());
    buffer.put(getMessage());
  }
  
  public int serializationSize() {
    return 1+4+4+getMessage().length;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int keyNumber = buffer.getInt();
    int packetNumber = buffer.getInt();
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new AckPacket(keyNumber,packetNumber,message);
  }


  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getAck().accept(this);
  }
  
  

}
