package xxx.easypto.priv.packet;

import java.security.PublicKey;

public interface PublicKeyPacket {
  int getDhKey();
  PublicKey getKey();
  byte[] getIV();
}
