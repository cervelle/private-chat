package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;

public class ErrPacket extends Packet {

  @Override
  public PacketType getType() {
    return PacketType.ERR;
  }

  public ErrPacket(int lastDecryptedMessage, int receivedMessage) {
    this.lastDecryptedMessage = lastDecryptedMessage;
    this.receivedMessage = receivedMessage;
  }
  
  

  public int getLastDecryptedMessage() {
    return lastDecryptedMessage;
  }

  public int getReceivedMessage() {
    return receivedMessage;
  }



  private final int lastDecryptedMessage;
  private final int receivedMessage;

  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(lastDecryptedMessage);
    buffer.putInt(receivedMessage);
  }

  public int serializationSize() {
    return 1+4+4;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int lastDecryptedMessage = buffer.getInt();
    int receivedMessage = buffer.getInt();
    return new ErrPacket(lastDecryptedMessage, receivedMessage);
  }

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getErr().accept(this);
  }

}
