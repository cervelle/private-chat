package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;

public enum PacketType {
  HELLO(1, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return HelloPacket.decodeImpl(buffer);
    }
  }),MSG(2, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return MsgPacket.decodeImpl(buffer);
    }
  }),ERR(3, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return ErrPacket.decodeImpl(buffer);
    }
  }),
  RENEGO(4, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return RenegoPacket.decodeImpl(buffer);
    }
  }),ACK(5, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return AckPacket.decodeImpl(buffer);
    }
  }),RENEGO_ACK(6, new PacketFactory() {
    @Override
    public Packet decode(ByteBuffer buffer) throws MalformedPacketException {
      return RenegoAckPacket.decodeImpl(buffer);
    }
  });
  
  PacketType(int code, PacketFactory factory) {
    this.code=(byte)code;
    this.factory=factory;
  }
  public byte getCode() {
    return code;
  }
  public PacketFactory getFactory() {
    return factory;
  }
  
  public static PacketType ofCode(byte b) {
    for(PacketType p:values()) {
      if(p.getCode()==b)
        return p;
    }
    return null;
  }
  
  private final byte code;
  private final PacketFactory factory;
}
