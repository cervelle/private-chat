package xxx.easypto.priv;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
//import android.util.Base64;
import java.util.Base64;
import java.util.regex.Pattern;

import xxx.easypto.priv.packet.MalformedPacketException;
import xxx.easypto.priv.packet.Packet;

public class B64Helper {
  
  private static final Pattern pattern = Pattern.compile("\\p{Space}*<\\*>[\\p{Space}a-zA-Z0-9/+]*[\\p{Space}=]*<\\*>\\p{Space}*");
  
  public static boolean isCoding(String s) {
    return pattern.matcher(s).matches();
  }
  
  public static String serializePacket(Packet p) {
    byte[] array = new byte[p.serializationSize()];
    p.encode(ByteBuffer.wrap(array));
    String base64 = Base64.getEncoder().encodeToString(array);
    return "<*>"+base64+"<*>";
  }
  
  public static Packet deserializePacket(String s) throws MalformedPacketException {
    if (!isCoding(s))
      throw new IllegalArgumentException();
    s=s.replaceAll("\\p{Space}", "");
    s=s.substring(3,s.length()-3);

    try {
      return Packet.decode(ByteBuffer.wrap(Base64.getDecoder().decode(s)));
    }
    catch (IllegalArgumentException | BufferUnderflowException e) {
      throw new MalformedPacketException(e);
    }
  }
}
