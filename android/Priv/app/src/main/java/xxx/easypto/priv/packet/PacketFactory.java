package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;

public interface PacketFactory {
  Packet decode(ByteBuffer buffer) throws MalformedPacketException;
}
