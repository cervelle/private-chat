package xxx.easypto.priv.protocol;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;

import xxx.easypto.priv.packet.AckPacket;
import xxx.easypto.priv.packet.Consumer;
import xxx.easypto.priv.packet.DataPacket;
import xxx.easypto.priv.packet.ErrPacket;
import xxx.easypto.priv.packet.HelloPacket;
import xxx.easypto.priv.packet.MsgPacket;
import xxx.easypto.priv.packet.Packet;
import xxx.easypto.priv.packet.PacketVisitor;
import xxx.easypto.priv.packet.PublicKeyPacket;
import xxx.easypto.priv.packet.RenegoAckPacket;
import xxx.easypto.priv.packet.RenegoPacket;

public class PacketManagement {
  private int negoPacketNumber;
  private int packetNumber;
  private int lastReceived;
  private long negoTime;

  private boolean handshakeCalled;

  private enum RenegoState {
    DEFAULT, RENEGO_SENT, ACK_TO_BE_SENT, RENEGO_ACK_TO_BE_SENT, ACK_EXPECTED, CROSS_RENEGO
  }

  private RenegoState renegoState = RenegoState.DEFAULT;

  private SecretKey key;
  private int keyNumE;
  private int keyNumI;
  private byte[] ivEgress, ivIngress;
  private PrivateKey privateKey;
  private PublicKey publicKey;
  private int privateDHKey;
  private int dhKey;
  private Cipher cipherIngress;
  private Cipher cipherEgress;
  private boolean helloJustSent=false;

  private Consumer<Packet> emit;
  private Consumer<String> print;

  private final PacketVisitor visitor = new PacketVisitor(new Consumer<HelloPacket>() {
    @Override
    public void accept(HelloPacket helloPacket) {
      receiveHello(helloPacket);
    }
  },
          new Consumer<MsgPacket>() {
            @Override
            public void accept(MsgPacket msgPacket) {
              receiveMsg(msgPacket);
            }
          }
          ,
          new Consumer<ErrPacket>() {
            @Override
            public void accept(ErrPacket errPacket) {
              receiveErr(errPacket);
            }
          },
          new Consumer<RenegoPacket>() {
            @Override
            public void accept(RenegoPacket renegoPacket) {
              receiveRenego(renegoPacket);
            }
          },
          new Consumer<RenegoAckPacket>() {
            @Override
            public void accept(RenegoAckPacket renegoAckPacket) {
              receiveRenegoAck(renegoAckPacket);
            }
          },
          new Consumer<AckPacket>() {
            @Override
            public void accept(AckPacket ackPacket) {
              receiveAck(ackPacket);
            }
          }

  );

  /* API */

  public void init(Consumer<Packet> emit, Consumer<String> print) {
    this.emit = emit;
    this.print = print;
    ivEgress = Crypto.emptyIV();
    packetNumber = Crypto.nextInt();
  }

  public void receive(Packet packet) {
    debug("recv "+packet.getType().name());    
    packet.accept(visitor);
  }

  /* utils */

  private void emit(Packet packet) {
    debug("send "+packet.getType().name());
    emit.accept(packet);
  }

  private void print(String s) {
    print.accept(s);
  }

  private static final Charset UTF_8;

  static {
    try {
      UTF_8 = Charset.forName("UTF8");
    }
    catch (UnsupportedCharsetException e) {
      throw new AssertionError("JVM badly installed");
    }
  }

  private byte[] encodeUTF8(String s) {
    ByteBuffer buffer = UTF_8.encode(s);
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return message;
  }

  /* crypto */

  private void computeKey() {
    key = Crypto.makeKey(privateKey, publicKey);
    debug("Key "+(key.getEncoded()[0]&0xFF)+" keyNum "+Crypto.keyNum(privateDHKey, dhKey));
  }

  private void setCipherEgress() {
    keyNumE = Crypto.keyNum(privateDHKey, dhKey);
    debug("E "+keyNumE+" "+(ivEgress[0]&0xFF));
    cipherEgress = Crypto.initCipher(key, ivEgress, Cipher.ENCRYPT_MODE);
  }

  private void setCipherIngress() {
    keyNumI = Crypto.keyNum(privateDHKey, dhKey);
    debug("I "+keyNumI+" "+(ivIngress[0]&0xFF));
    cipherIngress = Crypto.initCipher(key, ivIngress, Cipher.DECRYPT_MODE);
  }
  
  private void extractKeys(PublicKeyPacket packet) {
    ivIngress = packet.getIV();
    publicKey = packet.getKey();
    dhKey = packet.getDhKey();
  }

  private boolean receiveData(DataPacket packet, boolean last) {
    helloJustSent=false;
    byte[] cipheredMessage = packet.getMessage();
    int number = packet.getPacketNumber();
    int keyNum = packet.getKeyNumber();
    if (keyNum != this.keyNumI) {
      print("/!\\ Key exchange failed, renegociating /!\\");
      key=null;
      privateKey=null;
      publicKey = null;
      cipherIngress = null;
      cipherEgress = null;
      performHandshake();
      return false;
    }
    if (number != lastReceived + 1) {
      /* throw new UnsupportedOperationException(
          "Received packet " + number + " expecting " + (lastReceived + 1)); */
      print("/!\\ Misssing packets /!\\");
      emit(new ErrPacket(lastReceived,number));
      return false;
    }
    lastReceived++;
    byte[] message;
    if (last)
        message = decipher(cipheredMessage,true);
    else
      message = decipher(cipheredMessage,false);
    print(UTF_8.decode(ByteBuffer.wrap(message)).toString());
    return true;
  }

  /* Default message part */

  public void receiveMsg(MsgPacket packet) {
    receiveData(packet, false);
  }

  /* Hello part */

  public void startHandshake() {
    if (handshakeCalled)
      throw new ProtocolErrorException("Starting handshake twice");
    handshakeCalled = true;
    performHandshake();
  }

  private void performHandshake() {
    if (key != null) {
      /*
       * startHandShake called while nego already finished because Hello packet
       * was received
       */
      return;
    }

    if (privateKey != null)
      throw new AssertionError();

    KeyPair pair = Crypto.generateKeyPair();
    negoPacketNumber = packetNumber;
    negoTime = System.currentTimeMillis();
    privateKey = pair.getPrivate();
    privateDHKey = Crypto.nextInt();
    Crypto.fillIV(ivEgress);
    emit(new HelloPacket(Crypto.dhKey(privateDHKey),packetNumber, pair.getPublic(), ivEgress));
    helloJustSent = true;
  }

  public void receiveHello(HelloPacket packet) throws ProtocolErrorException {
    if (key != null && !helloJustSent) {
      //throw new ProtocolErrorException("Received new handshake");
      print("/!\\ other peer is restarting the protocol, messages may be lost /!\\");
      renegoState = RenegoState.DEFAULT;
      key = null;
      privateKey = null;
      publicKey = null;
      cipherIngress = null;
      cipherEgress = null;
    }
    helloJustSent=false;

    if (privateKey == null) { /*
                               * packet received before startHandShake is called
                               */
      performHandshake();
    }
    lastReceived = packet.getPacketNumber();
    extractKeys(packet);
    computeKey();
    setCipherEgress();
    setCipherIngress();
  }

  /* Renego part */
  
  public void receiveRenego(RenegoPacket packet) {
    if (!receiveData(packet, false))
      return;
    extractKeys(packet);
    
    switch (renegoState) {
    case RENEGO_SENT:
      renegoState = RenegoState.CROSS_RENEGO;
      computeKey();
      break;
    case DEFAULT:
      renegoState = RenegoState.RENEGO_ACK_TO_BE_SENT;
      break;
    default:
      throw new ProtocolErrorException(renegoState.name());
    }


  }

  public void receiveRenegoAck(RenegoAckPacket packet) {
    if (!receiveData(packet, true))
        return;

    if (renegoState != RenegoState.RENEGO_SENT)
      throw new ProtocolErrorException();
    renegoState = RenegoState.ACK_TO_BE_SENT;

    extractKeys(packet);
    computeKey();
    setCipherIngress();
  }

  public void receiveAck(AckPacket ack) {
    switch (renegoState) {
    case CROSS_RENEGO:
      if (!receiveData(ack, false))
        return;
      renegoState = RenegoState.ACK_TO_BE_SENT;
      setCipherIngress();
      break;
    case ACK_EXPECTED:
      if (!receiveData(ack, true))
        return;
      renegoState = RenegoState.DEFAULT;
      setCipherIngress();
      break;
    default:
      throw new ProtocolErrorException();
    }
  }

  /* Error handling */

  public void receiveErr(ErrPacket packet) {
    helloJustSent=false;
    int x = packet.getReceivedMessage()-packet.getLastDecryptedMessage()-1;
    if (x==1)
      print("/!\\ lacking 1 message /!\\");
    else
      print("/!\\ lacking "+x+" messages /!\\");
  }

  private static int blockAlign(int x) {
    int blocksize = Crypto.keySize()/8;
    return ((x+blocksize-1)/blocksize)*blocksize;
  }

  private byte[] cipher(byte[] data, boolean finalCall) {
    return docrypto(data,true,finalCall);
  }

  private byte[] decipher(byte[] data, boolean finalCall) {
     return docrypto(data,false,finalCall);
  }

  private byte[] docrypto(byte[] data, boolean egress, boolean finalCall) {
    byte[] enlarge = Arrays.copyOf(data,blockAlign(data.length));
    Cipher cipher = egress?cipherEgress:cipherIngress;
    if (finalCall) {
      try {
        enlarge = cipher.doFinal(enlarge);
      } catch (IllegalBlockSizeException | BadPaddingException e) {
        throw new ProtocolErrorException(e);
      }
    }
    else
      enlarge = cipher.update(enlarge);
    return Arrays.copyOf(enlarge,data.length);
  }

  public void sendString(String s) {
    if (s.length()==0)
      return;
    debug("DBG : send "+s);
    if (!handshakeCalled) {
      throw new IllegalStateException("startHandshake not called");
    }
    if (key==null) {
      print("First handshake packet not received");
      return;
    }

    byte[] message = encodeUTF8(s);
    if (message.length==0)
      return;
    byte[] cipheredMessage;
    helloJustSent=false;
    switch(renegoState) {
    case DEFAULT:
    case RENEGO_SENT:
    case ACK_EXPECTED:
      cipheredMessage = cipher(message,false);
      if (cipheredMessage==null)
        throw new AssertionError();
      break;
    case ACK_TO_BE_SENT:
    case CROSS_RENEGO:
    case RENEGO_ACK_TO_BE_SENT:
        cipheredMessage = cipher(message,true);
        if (cipheredMessage==null)
          throw new AssertionError();
      break;
    default:
      throw new AssertionError();
    }
    packetNumber++;
    int previousKeyNumE = keyNumE;
    
    switch(renegoState) {
    case DEFAULT:
      if (renegociationWanted()) {
        KeyPair pair = Crypto.generateKeyPair();
        negoPacketNumber = packetNumber;
        negoTime = System.currentTimeMillis();
        privateKey = pair.getPrivate();
        privateDHKey = Crypto.nextInt();
        Crypto.fillIV(ivEgress);
        renegoState = RenegoState.RENEGO_SENT;
        emit(new RenegoPacket(keyNumE,packetNumber, Crypto.dhKey(privateDHKey),pair.getPublic() , ivEgress, cipheredMessage));
        break;
      }
      // else proceed to default behavior identical to the next two cases
    case RENEGO_SENT:
    case ACK_EXPECTED:
      emit(new MsgPacket(keyNumE,packetNumber, cipheredMessage));
      break;
    case ACK_TO_BE_SENT:
      setCipherEgress();
      renegoState = RenegoState.DEFAULT;
      emit(new AckPacket(previousKeyNumE,packetNumber, cipheredMessage));
      break;
    case CROSS_RENEGO:
      setCipherEgress();
      renegoState = RenegoState.ACK_EXPECTED;
      emit(new AckPacket(previousKeyNumE,packetNumber, cipheredMessage));
      break;
    case RENEGO_ACK_TO_BE_SENT:
      KeyPair pair = Crypto.generateKeyPair();
      negoPacketNumber = packetNumber;
      negoTime = System.currentTimeMillis();
      privateKey = pair.getPrivate();
      privateDHKey = Crypto.nextInt();
      Crypto.fillIV(ivEgress);
      computeKey();
      setCipherEgress();
      renegoState = RenegoState.ACK_EXPECTED;
      emit(new RenegoAckPacket(previousKeyNumE,packetNumber, Crypto.dhKey(privateDHKey),pair.getPublic() , ivEgress,cipheredMessage));
      break;
    default:
       throw new AssertionError(); 
    }
    
  }
  
  private void debug(String s) {
    print("DBG : "+s+" ["+renegoState+"]");
  }

  private boolean renegociationWanted() {
    /* renegociate key every 4000 messages */
    boolean renego = (long) (packetNumber) - (long) (negoPacketNumber) > 2L;
    /* renegociate key every hours */
    renego = renego || (System.currentTimeMillis() - negoTime)>1000*60*60;
    if (renego)
     debug("renego");
    return renego;
  }

}
