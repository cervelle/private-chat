package xxx.easypto.priv;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import xxx.easypto.priv.packet.Consumer;
import xxx.easypto.priv.packet.MalformedPacketException;
import xxx.easypto.priv.packet.Packet;
import xxx.easypto.priv.protocol.PacketManagement;

public class PrivActivity extends AppCompatActivity {

    public static PrivActivity act;

    public void print(String s) {
        TextView tv = (TextView)findViewById(R.id.out);
        tv.append(s+"\n");
        ClipData clip = ClipData.newPlainText("simple text",s);
        ClipboardManager clipboard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(clip);
    }

    public void processLine() {
        EditText et = (EditText) findViewById(R.id.in);
        String s = et.getText().toString();
        print(s);
        et.getText().clear();
        try {
            if (B64Helper.isCoding(s))
                pm.receive(B64Helper.deserializePacket(s));
            else
                pm.sendString(s);
        }
        catch (MalformedPacketException e) {
            print("/!\\ Malformed packet /!\\");
        }
    }

    public void processButton() {
        ClipboardManager clipboard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData data = clipboard.getPrimaryClip();
        String s = data.getItemAt(0).coerceToText(this).toString();
        print(s);
        /*String s = et.getText().toString();
        et.getText().clear(); */
        try {
            if (B64Helper.isCoding(s))
                pm.receive(B64Helper.deserializePacket(s));
            else
                pm.sendString(s);
        }
        catch (MalformedPacketException e) {
            print("/!\\ Malformed packet /!\\");
        }
    }

    PacketManagement pm = new PacketManagement();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        act=this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pm.init(new Consumer<Packet>() {
                    @Override
                    public void accept(Packet packet) {
                        print("    " + B64Helper.serializePacket(packet));
                    }
                },
                new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        print(s);
                    }
                });
        pm.startHandshake();

        //findViewById(R.id.in);
        //findViewById(R.id.out);

        EditText et = (EditText) findViewById(R.id.in);
        TextView.OnEditorActionListener l = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    processLine();
                }
                catch (Throwable t) {
                    print(t.toString());
                    for(StackTraceElement st : t.getStackTrace())
                        print(st.toString());
                }
                return true;
            }
        };
        et.setOnEditorActionListener(l);


        Button btn = (Button) findViewById(R.id.btn);
        Button.OnClickListener l2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    processButton();
                }
                catch (Throwable t) {
                    print(t.toString());
                    for(StackTraceElement st : t.getStackTrace())
                        print(st.toString());
                }
            }
        };
        btn.setOnClickListener(l2);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
