package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;
import java.security.PublicKey;

import xxx.easypto.priv.protocol.Crypto;

public class HelloPacket extends Packet implements PublicKeyPacket {

  @Override
  public PacketType getType() {
    return PacketType.HELLO;
  }
  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(dhKey);
    buffer.putInt(packetNumber);
    buffer.put(iv);
    byte[] keyBytes = key.getEncoded();
    buffer.put(keyBytes);
  }

  public int serializationSize() {
    return 1+4+4+iv.length+key.getEncoded().length;
  }
  
  public HelloPacket(int dhKey, int packetNumber, PublicKey key, byte[] iv) {
    this.packetNumber = packetNumber;
    this.dhKey = dhKey;
    this.key = key;
    this.iv=iv.clone();
  }

  public PublicKey getKey() {
    return key;
  }
  
  public byte[] getIV() {
    return iv;
  }
  
  public int getPacketNumber() {
    return packetNumber;
  }
  
  public int getDhKey() {
    return dhKey;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int dhKey = buffer.getInt();
    int packetNumber = buffer.getInt();
    byte[] iv = Crypto.emptyIV();
    buffer.get(iv);
    byte[] keyBytes = new byte[buffer.remaining()];
    buffer.get(keyBytes);
    return new HelloPacket(dhKey,packetNumber,Crypto.decodeKey(keyBytes),iv);
  }

  private final PublicKey key;
  private final byte[] iv;
  private final int packetNumber;
  private final int dhKey;

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getHello().accept(this);
  }

}
