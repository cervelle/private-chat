package xxx.easypto.priv.packet;

import java.nio.ByteBuffer;
import java.security.PublicKey;

import xxx.easypto.priv.protocol.Crypto;

public class RenegoPacket extends DataPacket implements PublicKeyPacket {

  @Override
  public PacketType getType() {
    return PacketType.RENEGO;
  }
  
  @Override
  public void encodeImpl(ByteBuffer buffer) {
    buffer.putInt(getKeyNumber());
    buffer.putInt(getPacketNumber());
    buffer.putInt(dhKey);
    buffer.put(iv);
    byte[] keyBytes = key.getEncoded();
    buffer.put((byte)keyBytes.length);
    buffer.put(keyBytes);
    buffer.put(getMessage());
  }

  public int serializationSize() {
    return 1+4+4+4+iv.length+1+key.getEncoded().length+getMessage().length;
  }
  
  public RenegoPacket(int keyNumber, int packetNumber, int dhKey, PublicKey key, byte[] iv, byte[] message) {
    super(keyNumber,packetNumber,message);
    this.key = key;
    this.iv=iv.clone();
    this.dhKey=dhKey;
  }

  public PublicKey getKey() {
    return key;
  }
  
  public byte[] getIV() {
    return iv;
  }
  
  public int getDhKey() {
    return dhKey;
  }
  
  public static Packet decodeImpl(ByteBuffer buffer) throws MalformedPacketException {
    int keyNumber = buffer.getInt();
    int packetNumber = buffer.getInt();
    int dhKey = buffer.getInt();
    byte[] iv = Crypto.emptyIV();
    buffer.get(iv);
    int keyLength = buffer.get();
    byte[] keyBytes = new byte[keyLength];               
    buffer.get(keyBytes);
    byte[] message = new byte[buffer.remaining()];
    buffer.get(message);
    return new RenegoPacket(keyNumber,packetNumber,dhKey,Crypto.decodeKey(keyBytes),iv,message);
  }

  private final int dhKey;
  private final PublicKey key;
  private final byte[] iv;

  @Override
  public void accept(PacketVisitor visitor) {
    visitor.getRenego().accept(this);
  }

}
